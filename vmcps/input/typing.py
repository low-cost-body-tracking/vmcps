#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: AGPL-3.0-or-later

"""``typing`` module of ``input`` package."""

from typing import (
    TypeVar,
    Literal
)
from abc import (
    ABCMeta,
    abstractmethod
)
from types import TracebackType
# Numpy
from numpy import ndarray

InputT = TypeVar("InputT", bound="InputType")


class InputType(metaclass=ABCMeta):
    """Abstract input type."""

    @property
    @abstractmethod
    def is_open(self: InputT) -> bool:
        """bool: Returns whether it's currently open."""

    @property
    @abstractmethod
    def end_of_data(self: InputT) -> bool:
        """bool: Returns whether the input data reached its end."""

    @abstractmethod
    def open(self: InputT) -> InputT:
        """Open input.

        Returns:
            InputT:
                Instance.

        """

    def __enter__(self: InputT) -> InputT:
        """Return the object instance for usage of the ``with`` statement.

        Returns:
            InputT:
                Instance.

        """
        self.open()
        return self

    @abstractmethod
    def read(self: InputT) -> ndarray:
        """Return the latest input data.

        Returns:
            ndarray:
                Data.

        """

    @abstractmethod
    def close(self: InputT) -> None:
        """Close input."""

    def __exit__(
        self: InputT,
        exc_type: type[BaseException] = None,
        exc_value: BaseException = None,
        traceback: TracebackType = None
    ) -> Literal[False]:
        """Return everytime ``False`` if exiting the runtime context.

        Args:
            exc_type (Optional[type[BaseException]]):
                Exception type.
            exc_value (Optional[BaseException]):
                Exception instance.
            traceback (Optional[TracebackType]):
                Exception context.

        Returns:
            bool:
                Everytime ``False``.

        """
        self.close()
        return False
