# **VMC** (Virtual Motion Capture) **p**rotocol **s**upport for different Motion Capture solutions
[![License: AGPL-3.0-or-later](https://img.shields.io/badge/%E2%9A%96%EF%B8%8F-AGPL%203.0%20or%20later-green)](https://www.gnu.org/licenses/agpl.html)
[![State: Stable](https://img.shields.io/badge/State-Stable-green.svg)](https://codeberg.org/vivi90/python-vmc/issues)

The [VMC protocol](https://protocol.vmc.info/english) is used for 
realtime Motion Capture transmission and based on 
the [Open Sound Control (OSC) protocol](https://opensoundcontrol.stanford.edu).

This project might become an collection of VMCP implementations for different Motion Capture solutions.
For now it provides only an working draft for the [ROMP](https://github.com/Arthur151/ROMP) model.


## Install

### Common
For every implementation you require: `pip install pyyaml vmcp[osc4py3]`

### Different implementations
 - __[ROMP](https://github.com/Arthur151/ROMP)__

     Additionally you require: 
     ```
     pip install torch torchvision torchaudio --extra-index-url https://download.pytorch.org/whl/cu116
     pip install numpy simple-romp cython
     ```
     *Note: Please choose the suitable CUDA Toolkit version for your used hardware (in the example: `11.6` for RTX 3060 Ti).*


## Usage

 - __[ROMP](https://github.com/Arthur151/ROMP)__
   1. Adjust [`romp.yml`](https://codeberg.org/vivi90/vmcps/src/branch/main/romp.yml) to your needs.
      | Option | Possible values |
      |--------|-----------------|
      | `source-type` | `image` or `camera` |
      | `source` | Device number or complete file path. |
      | `legacy` | `false` will improve the avatar scaling but requires that the receiving application supports VMC protocol version 2.1.0 or higher. |
   2. Run `python romp_vmcp.py`
   3. Since ROMP does NOT provide bone positions (3D translations), additional IK (Inverse kinematics) may be required. *(Note: In EVMC4U you just require to disable `Bone Position Synchronize` in your `ExternalReceiver` instance.)* 


## Project state meanings
 * **Unstable:** Still an *possibly* broken draft or breaking issues are known.
 * **Refactoring:** Structural and technical changes have currently priority.
 * **Stable:** Everything seems working as expected.
 * **Fixes only:** The project is maintained at the minimum level to apply at least fixes.
 * **Takeover:** The project is currently being taked over by another team and will possibly move.
 * **Not maintained:** The project is not maintained any more (ready to take over).

*Feature requests are welcomed all the time, of course! ;-)*


## License
This project is free under the terms of the AGPL 3.0 (or later) license. 
For more details please see the LICENSE file or: [GNU](https://www.gnu.org/licenses/agpl.html)
